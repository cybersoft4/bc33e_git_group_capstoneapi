//đặt biến để lấy giá trị từ API
var arrPro=[]

//đặt biến arr giỏ hàng
let cartItem=[]

//lưu giỏ hàng vào localStore
const saveLocalStorage=()=> {
    var ItemCartJson = JSON.stringify(cartItem);
    localStorage.setItem("itemInCart", ItemCartJson);
}

// bat/tat khi loading data
var batLoading =  () =>{
    document.getElementById("loading").style.display = "flex";
  };
var tatLoading =  () =>{
    document.getElementById("loading").style.display = "none";
  };

// ham render san pham 
var renderProducts = list => {
    let contentHTML = "";
    list.forEach(item =>{
      let content = `
      <div class="card">
        <img src=${item.img} class="card-img-top" alt="">
        <div class="card-body">
            <div style="height:150px">
                <h5 class="card-title">${item.name}</h5>
                <h4 class="card-title">$${item.price}</h4>
                <p class="card-text">${item.desc}</p>
            </div>
            <button class="btn btn-primary" onclick={handleAdd('${item.id}')}>Add to cart</button>
        </div>
      </div>`
      ;
      contentHTML += content;
    });
    document.querySelector("#showProducts").innerHTML = contentHTML;
}

//render sản phẩm trong giỏ hàng
const renderCartItem=(list)=>{
    let contentHTML = "";
    list.forEach(item =>{
    let content = `
        <tr>
            <td style="display:flex; justify-content:start;align-items:center">
                <img class="imgCartItem" src=${item.img} alt=""/>
                <p>${item.name}</p>
            </td>
            <td class="price">$${item.price*item.qty}</td>
            <td>
                <button class="btn btn-primary" onclick={tangSl('${item.id}')}>+</button>
                <span>${item.qty}</span>
                <button class="btn btn-danger" onclick={giamSl('${item.id}')}>-</button>
            </td>
            <td>
                <button class="btn btn-danger" onclick={removeItemCart('${item.id}')}>Remove</button>
            </td>
        </tr>
    `
    ;
    contentHTML += content;
});
    document.querySelector("#listProductCart").innerHTML = contentHTML;
}



//hàm lọc sản phẩm theo loại
const handleFilter=()=>{
    const ele = document.getElementById("type");
    var value = ele.options[ele.selectedIndex].value;
    axios({
        url: "https://62fb4582e4bcaf5351806be5.mockapi.io/Products",
        method: "GET",
    })
    .then(res=>{
        arrPro = res.data;
        if(value=="All"){
            renderProducts(arrPro)
        }else{
            const newArr=arrPro.filter((x)=>x.type.toLowerCase()===value)
            renderProducts(newArr)
        }
    })
}
//lấy dữ liệu từ API và gọi hàm render để render ra product
var renderAPIProduct =  () =>{
    batLoading();
    axios({
        url: "https://62fb4582e4bcaf5351806be5.mockapi.io/Products",
        method: "GET",
    })
    .then(res=>{
        tatLoading();
        arrPro = res.data;
        renderProducts(arrPro);
      })
    .catch( (err) =>{
        tatLoading();
        console.log(err);
    });
  };
renderAPIProduct();



//hàm thêm sản phẩm vào giỏ hàng
const handleAdd=(id)=>{
    batLoading()
    axios({
        url: "https://62fb4582e4bcaf5351806be5.mockapi.io/Products",
        method: "GET",
      })
    .then(res=>{
        tatLoading()
        arrPro = res.data;
        const index=arrPro.findIndex(x=>x.id===id)
        const item=arrPro[index]
        const newItem={...item,qty:1}
        const exist=cartItem.find(x=>x.id===id)
        if(exist){
            const newExist={...exist,qty:exist.qty+=1}
            const indexExist=cartItem.findIndex(x=>x.id===id)
            cartItem[indexExist]=newExist
            renderCartItem(cartItem)
        }else{
            cartItem.push(newItem)
            renderCartItem(cartItem)
        }
        updateItemCart(cartItem)
        totalPrice(cartItem)
        saveLocalStorage()
    })   
}

//tăng số lượng của sản phẩm trong giỏ hàng
const tangSl=(id)=>{
    const exist=cartItem.find(x=>x.id===id)
    const newExist={...exist,qty:exist.qty+=1}
    const indexExist=cartItem.findIndex(x=>x.id===id)
    cartItem[indexExist]=newExist
    renderCartItem(cartItem)
    totalPrice(cartItem)
    saveLocalStorage()
}

//giảm số lượng của sản phẩm trong giỏ hàng
const giamSl=(id)=>{
    const exist=cartItem.find(x=>x.id===id)
    const indexExist=cartItem.findIndex(x=>x.id===id)
    if(exist.qty>1){
        const newExist={...exist,qty:exist.qty=exist.qty-1}
        cartItem[indexExist]=newExist
        renderCartItem(cartItem)
        totalPrice(cartItem)
        saveLocalStorage()
    }else{
        removeItemCart(id)
        updateItemCart(cartItem)
        totalPrice(cartItem)
        saveLocalStorage()
    }
}

//xóa 1 sản phẩm trong giỏ hàng
const removeItemCart=(id)=>{
    const indexExist=cartItem.findIndex(x=>x.id===id)
    cartItem.splice(indexExist,1)
    renderCartItem(cartItem)
    updateItemCart(cartItem)
    totalPrice(cartItem)
    saveLocalStorage()
}

//hiển thị số lượng sản phẩm trong giỏ hàng
const updateItemCart=(list)=>{
    const numberItemCart=document.querySelector(".numberItemCart")
    numberItemCart.innerHTML=`(${list.length})` 
}

//tính giá của tổng hóa đơn
const totalPrice=(list)=>{
    const cartTotalPrice=document.querySelector(".cart-total-price")
    let totalPrice=0
    for(let i=0;i<list.length;i++){
        totalPrice+=list[i].price*list[i].qty*1
    }
    cartTotalPrice.innerHTML=`$${totalPrice}`
}

//hàm xử lý thanh toán 
const handleReset=()=>{
    cartItem=[]
    renderCartItem(cartItem)
    updateItemCart(cartItem)
    totalPrice(cartItem)
    alert("Đơn hàng của bạn đã được thanh toán")
    saveLocalStorage()
}
let storage = localStorage.getItem("itemInCart");
    if (storage) {
        cartItem = JSON.parse(storage);   
        renderCartItem(cartItem)
        totalPrice(cartItem)
        updateItemCart(cartItem)
   }else{
        cartItem=[]
        removeItemCart(cartItem)
   }
