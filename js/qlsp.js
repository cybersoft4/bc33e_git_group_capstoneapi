var dssp = [];
var BASE_URL = "https://62fb4582e4bcaf5351806be5.mockapi.io/";

var batLoading = function () {
  document.getElementById("loading").style.display = "flex";
};
var tatLoading = function () {
  document.getElementById("loading").style.display = "none";
};

var renderTable = function (list) {
  var contentHTML = "";
  list.forEach(function (item) {
    var trContent = `
   <tr>
    <td>${item.id}</td>
    <td>${item.name}</td>
    <td>${item.price}</td>
    <td>
       <img src=${item.img} style="width:80px"   alt="" />
    </td>
    <td>${item.desc}</td>
    <td style="display:flex">
      <button
      onclick="xoaSanPham('${item.id}')"
      class="btn btn-danger">Xoá</button>
      <button
      onclick="suaSanPham('${item.id}')"
      class="btn btn-warning">Sửa</button>
    </td>
  </tr>
    `;
    contentHTML += trContent;
  });
  document.getElementById("tbodySanPham").innerHTML = contentHTML;
};

var renderDSSPService = function () {
    batLoading();
    axios({
    url: `${BASE_URL}/Products`,
    method: "GET",
  })
    .then(function (res) {
      tatLoading();
      dssp = res.data;
      renderTable(dssp);
      resetForm()
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
};
renderDSSPService()

const ADDSP=()=>{
    var dataForm = layThongTinTuForm();
    var isValid=validator.kiemtraRong(dataForm.name,"spanTenSP","không được để trống trường này")
    &validator.kiemtraRong(dataForm.price,"spanGiaSP","không được để trống trường này")
    &validator.kiemtraRong(dataForm.img,"spanImageSP","không được để trống trường này")
    &validator.kiemtraRong(dataForm.desc,"spanDescSP","không được để trống trường này")
        if(isValid==false){
          return
        }
    batLoading();
    axios({
      url: `${BASE_URL}/Products`,
      method: "POST",
      data: dataForm,
    })
    .then(function (res) {
        tatLoading();
        
        renderDSSPService();
    })
    .catch(function (err) {
        tatLoading();
        console.log(err);
    });
}

const xoaSanPham=(id)=>{
    batLoading();
    axios({
      url: `${BASE_URL}/Products/${id}`,
      method: "DELETE",
    })
    .then(function (res) {
        tatLoading();
        renderDSSPService();
    })
    .catch(function (err) {
        tatLoading();
        console.log(err);
    });
}

const suaSanPham=(id)=>{
    batLoading();
    axios({
      url: `${BASE_URL}/Products/${id}`,
      method: "GET",
    })
    .then(function (res) {
        tatLoading();
        showThongTinLenForm(res.data);
    })
    .catch(function (err) {
        tatLoading();
        console.log(err);
    });
}
const capNhatSP=()=>{
    var dataForm = layThongTinTuForm();
    console.log(dataForm)
    batLoading();
    axios({
        url: `${BASE_URL}/Products/${dataForm.id}`,
        method: "PUT",
        data: dataForm,
    })
    .then(function (res) {
        tatLoading();
        renderDSSPService();
    })
    .catch(function (err) {
        tatLoading();
        console.log(err);
    });
}

const resetForm=()=>{
    document.getElementById("formQLSP").reset()
}