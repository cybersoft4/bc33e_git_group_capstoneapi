
var validator={
    kiemtraRong:function(valueInput,idError,message){
        if(valueInput.trim()==""){
            document.getElementById(idError).innerHTML=message
            return false
        }else{
            document.getElementById(idError).innerHTML=""
            return true
        }
    },
    kiemtraMaSV:function(ma,arr){
        const index=arr.findIndex(sv=>{
            return sv.ma==ma
        })
        if(index!=-1){
            document.getElementById('spanMaSV').innerHTML="ID da ton tai"
            return false
        }else{
            document.getElementById('spanMaSV').innerHTML=""
            return true
        }
    },
    kiemtraLengh:function(valueInput,idError){
        if(valueInput.lengh>=5&&valueInput.lengh<=20){
            document.getElementById(idError).innerHTML=""
            return true
        }else{
            document.getElementById(idError).innerHTML="Do dai khong cho phep"
            return false
        }
    }
}