const  layThongTinTuForm=() =>{
    let id=document.getElementById("txtMaSP").value;
    let name = document.getElementById("txtTenSP").value;
    let price = document.getElementById("txtGiaSP").value;
    let img = document.getElementById("txtImg").value;
    let desc = document.getElementById("txtDesc").value;
    return {
      id:id,
      name:name,
      price:price,
      img:img,
      desc:desc
    };
}
  
const showThongTinLenForm=data=> {
    document.getElementById("txtMaSP").value = data.id;
    document.getElementById("txtTenSP").value = data.name;
    document.getElementById("txtGiaSP").value = data.price;
    document.getElementById("txtImg").value = data.img;
    document.getElementById("txtDesc").value = data.desc;
}